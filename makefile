# App level nonsense
# 
git:
	ansible-playbook playbook_remote_git.yml -i hosts --tags deploy
search:
	ansible-playbook playbook_remote_search.yml -i hosts --tags deploy
fish:
	ansible-playbook playbook_remote_fishnet.yml -i hosts --tags deploy
opennic:
	ansible-playbook playbook_remote_opennic.yml -i hosts --skip-tags iptables --ask-become-pass
update:
	ansible-playbook playbook_remote_update.yml --forks 5 -i hosts --ask-become-pass
mail:
	ansible-playbook playbook_remote_mailcow.yml -i hosts --ask-become-pass
0x1a:
	ansible-playbook playbook_remote_0x1a.yml -i hosts --tags deploy
linx:
	ansible-playbook playbook_remote_linx.yml -i hosts --tags deploy
minio:
	ansible-playbook playbook_remote_minio.yml -i hosts
archiveteam:
	ansible-playbook playbook_remote_archiveteam.yml -i hosts --tags deploy
games:
	ansible-playbook playbook_games.yml -i hosts
omnivore:
	ansible-playbook playbook_remote_omnivore.yml -i hosts 
wallabag:
	ansible-playbook playbook_remote_wallabag.yml -i hosts
status:
	ansible-playbook playbook_remote_status.yml -i hosts --ask-become-pass --tags deploy

# Code we want to run occasionally
disk-check:
	ansible all --forks 5 -a 'df -h /' -i hosts
docker-clean:
	ansible all --forks 5 -a 'docker system prune -a' -i hosts --become --ask-become-pass

#!/bin/sh

# Change these two lines:
sender="root@`hostname`"
recepient="alerts@slowb.ro"

if [ "$PAM_TYPE" != "close_session" ]; then
    host="`hostname`"
    subject="SSH Login: $PAM_USER from $PAM_RHOST on $host"
    # Message to send, e.g. the current environment variables.
    message="`env`"
    echo "$message" | mailx -s "$subject" "$recepient"
    #echo "$message" | mailx -r "$sender" -s "$subject" "$recepient"
fi

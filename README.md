# Ansible Configuration Management for Slowb.ro

![The current build status (it should always be green...)](https://woodpecker.slowb.ro/api/badges/slowb.ro/server-slowb.ro/status.svg)

The showcases our configuration management system utilised by slowb.ro. 

Our tech stack consists of:
 - Ansible (for configuration management)
 - Docker (for running all the contains)
 - debian/ubuntu (for underlying hosting)

Using the tech stack above enables slowbro to have:
 - acme.sh to generate wildcard LE certs via he.net for DNS
 - traefik for web ingress, routing, & authentication
 - mailcow for self-hosted e-mail
 - wallabag as a read-it-later app 
 - gitea to host this git
   - woodpecker-ci for automated testing (& deployments)
   - image/registry for our own builds 
 - mumble for instant voice communications
 - nginx for website hosting (https://blog.slowb.ro)
 - tor relay(s) & onion(s) for a secure & libre internet
 - (Suggest what else I should use)

Secrets are pulled in from [pass](https://www.passwordstore.org)

## App Deployment(s)

If you have a copy of this repository and wish to build any of these services/tech stack you will need to do a couple things before deploying:
- Install ansible & pass on your local machine and setup the secrets needed.
- Make changes to the defaults under `roles/{role}/defaults/main.yml` in nearly all roles.
  - You can find what path we look for secrets in there.
- Create your own hosts file with the servers.

Once that is done all you need is SSH access and you are ready to deploy.


### Mailcow
After rolling my own role, mailcow actually provide an ansible role under `mailcow.mailcow`. So we have migrated over to that.
We recommend at a minimum 4GB of ram for your mailserver. Due to rspamd (anti-spam) & clamav (anti-virus), they take up about 600M & 1.3G respectively. 
```
ansible-playbook playbook_remote_mailcow.yml -i hosts --ask-become-pass
```

### Wallabag
A great read-it-later application with solid android support. 

```
ansible-playbook playbook_remote_wallabag.yml -i hosts
```

### Git (Gitea/Woodpecker/Docker Registry)
```
ansible-playbook playbook_remote_git.yml -i hosts
```

### Mumble
```
ansible-playbook playbook_remote_mumble.yml -i hosts
```

---- 

Extra Services:


### Fishnet
Fishnet is a distributed service to help lichess.org run their analysis of the Stockfish engine.
We contribute our excess cpu to help others improve their performace. Read more about Fishnet here: https://github.com/niklasf/fishnet#readme

```
ansible-playbook playbook_remote_fishnet.yml -i hosts
```

### OpenNic
We contribute back to the [OpenNIC Project](https://www.opennic.org) with multiple [Tier 2 DNS](https://wiki.opennic.org/opennic/tier2setup) resolvers.

The role utilises:
- [srvzone](https://wiki.opennic.org/opennic/srvzone) method for automating BIND9 updates
- Has zero logging by default
- Integrates the [Tier 2 Security Measures](https://wiki.opennic.org/opennic/tier2security) via iptables for some minor form of security

We will be setting up dnscrypt/DoH/DoT in the future

#### Note:
If you utilise this role, it will remove all iptable INPUT rules you have and replace then with only allowing SSH & DNS queries. Use at your own risk!
I welcome a more efficient solution for this problem. If you have any ideas please feel free to contact me. 

```
ansible-playbook playbook_remote_opennic.yml -i hosts
```

### Tor
We run tor relays on nearly all systems to contribute back our excess bandwidth
If you use our common role, you are also contributing!
